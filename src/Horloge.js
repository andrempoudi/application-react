import React from 'react';

/**
 * Composant Horloge
 */
class Horloge extends React.Component {
    d = new Date();

    constructor(props) {
        super(props);
        this.state = {
            heure: addZero(this.d.getHours()),
            minutes: addZero(this.d.getMinutes()),
            daily: this.d.toLocaleTimeString()
        };
        //Liaison des fonctions afin d'utiliser this en JSX
        this.setNewTimeFromTemps = this.setNewTimeFromTemps.bind(this);
    }

    /**
     Fonction permettant de modifier l'heure
     */
    setNewTimeFromTemps = (dataFromTemps) => {
        this.d.setHours(dataFromTemps.heure);
        this.d.setMinutes(dataFromTemps.minutes);
        this.d.setDate(dataFromTemps.jour);
        this.d.setMonth(dataFromTemps.mois);
        this.d.setFullYear(dataFromTemps.annee);

        let newHours = {
            heure: this.d.getHours(),
            minutes: this.d.getMinutes(),
            time: this.d.toLocaleTimeString()
        };

        this.setState({
            heure: addZero(newHours.heure),
            minutes: addZero(newHours.minutes),
            daily: newHours.time
        });
    }

    /**
     Fonction permettant de modifier la date
     */
    setNewDate = (date) => {
        this.d.setDate(date.jour);
        this.d.setMonth(date.mois);
        this.d.setFullYear(date.annee);

        this.setState({
            daily: this.d.toLocaleTimeString()
        });
    }

    /**
     Fonction permettant de modifier l'heure
     */
    setNewHour = (hours) => {
        this.d.setHours(hours);

        this.setState({
            heure: addZero(this.d.getHours())
        });
    }

    /**
     Fonction permettant de modifier les minutes
     */
    setNewMinutes = (minutes) => {
        this.d.setMinutes(minutes);

        this.setState({
            minutes: addZero(this.d.getMinutes())
        });
    }

    render() {
        return (
            <div>
                <Jour day={this.d.getDay()}/>
                <Periode hour={this.state.heure}/>
                <h1> {this.state.heure + ' : ' + this.state.minutes} </h1>
                <MaDate date={this.d}/>
                <UpdateDate onClick={this.setNewDate}/>
                <UpdateHour onClick={this.setNewHour}/>
                <UpdateMinutes onClick={this.setNewMinutes}/>
            </div>
        );
    }
}

/**
 * Component UpdateDate
 */
class UpdateDate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};

        /* Events */
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleSubmitDate = this.handleSubmitDate.bind(this);
    }

    handleChangeDate(event) {
        this.setState({value: event.target.value});
    }

    onClick = () => {
        let date = this.state.value;
        //Explosion de la chaine pour obtenir l'année, le mois et le jour
        date = date.split('-');

        this.props.onClick({
            jour: date[2],
            mois: date[1] - 1,
            annee: date[0]
        });
    };

    handleSubmitDate(event) {
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmitDate}>
                    <label>
                        Date :
                        <input type="date" name="date" value={this.state.value} onChange={this.handleChangeDate}/>
                    </label>
                    <button type="submit" onClick={this.onClick}>Modifier la date</button>
                </form>
            </div>
        );
    }
}

/**
 * Component UpdateHour
 */
class UpdateHour extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};

        /* Events */
        this.handleChangeHour = this.handleChangeHour.bind(this);
        this.handleSubmitHour = this.handleSubmitHour.bind(this);
    }

    handleChangeHour(event) {
        if (event.target.value > 23) this.setState({value: 23});
        else if (event.target.value < 0) this.setState({value: 0});
        else this.setState({value: event.target.value});
    }

    onClick = () => {
        this.props.onClick(this.state.value);
    };

    handleSubmitHour(event) {
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmitHour}>
                    <label>
                        Heure :
                        <input type="number" name="heure" value={this.state.value} onChange={this.handleChangeHour}/>
                    </label>
                    <button type="submit" onClick={this.onClick}>Modifier l'heure</button>
                </form>
            </div>
        );
    }
}

/**
 * Component UpdateMinutes
 */
class UpdateMinutes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};

        /* Events */
        this.handleChangeMinutes = this.handleChangeMinutes.bind(this);
        this.handleSubmitMinutes = this.handleSubmitMinutes.bind(this);
    }

    handleChangeMinutes(event) {
        if (event.target.value > 59) this.setState({value: 59});
        else if (event.target.value < 0) this.setState({value: 0});
        else this.setState({value: event.target.value});
    }

    onClick = () => {
        this.props.onClick(this.state.value);
    };

    handleSubmitMinutes(event) {
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmitMinutes}>
                    <label>
                        Minutes :
                        <input type="number" name="minutes" value={this.state.value} onChange={this.handleChangeMinutes}/>
                    </label>
                    <button type="submit" onClick={this.onClick}>Modifier les minutes</button>
                </form>
            </div>
        );
    }
}

/**
 * Composant MaDate
 */
function MaDate(props) {
    let today = new Date();
    let saveDate = {
        jour: props.date.getDate(),
        mois: props.date.getMonth(),
        annee: props.date.getFullYear()
    };

    let arrayOfMonths = [
        'Janvier', 'Février',
        'Mars', 'Avril',
        'Mai', 'Juin',
        'Juillet', 'Août',
        'Septembre', 'Octobre',
        'Novembre', 'Décembre'
    ];

    if (!props.date) {
        return (
            <h1>{arrayOfMonths[today.getMonth()]}</h1>
        );
    } else {
        return (
            <h1>
                {addZero(saveDate.jour) + ' ' + arrayOfMonths[saveDate.mois] + ' ' + saveDate.annee}
            </h1>
        )
    }
}

/**
 * Composant Periode
 * @param {*} props
 * @returns
 */
function Periode(props) {
    let period = '';
    if (props.hour >= 5 && props.hour < 12) {
        period = 'Matin';
    } else if (props.hour >= 13 && props.hour < 18) {
        period = 'Après-midi';
    } else {
        period = 'Soir';
    }

    return (
        <div>
            <h2> {period} </h2>
        </div>
    );
}

/**
 * Composant Jour
 * @param {*} props
 * @returns
 */
function Jour(props) {
    let today = new Date();
    let arrayOfDays = [
        'Dimanche',
        'Lundi',
        'Mardi',
        'Mercredi',
        'Jeudi',
        'Vendredi',
        'Samedi'
    ];

    if (!props.day) {
        return (
            <h1>{arrayOfDays[today.getDay()]}</h1>
        );
    } else {
        return (
            <h1>{arrayOfDays[props.day]}</h1>
        )
    }
}

/**
 Fonction permettant d'ajouter un zéro devant les chiffres de 0 à 9
 */
function addZero(nber) {
    if (nber < 10) {
        nber = "0" + nber;
    }
    return nber;
}

export default Horloge;