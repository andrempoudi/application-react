import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css';
import Horloge from './Horloge';

// ReactDOM.render(
//   <React.StrictMode>
//     <Liste />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

setInterval(() => {
  ReactDOM.render(
    <div className="App">
      <header className="App-header">
        <Horloge/>
      </header>
    </div>,
  document.getElementById('root')
);
}, 1000);
